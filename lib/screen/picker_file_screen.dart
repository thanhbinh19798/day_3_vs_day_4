import 'package:design_pattern_vs_view_advance/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class PickerFileScreen extends GetView<HomeController> {
  const PickerFileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        controller.permissionHelper(context);
      },
      child: Container(
        alignment: Alignment.center,
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          border: Border.all(width: 1),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset("assets/ic_picker_file.svg"),
            const SizedBox(
              height: 10,
            ),
            const Text("Thêm file",
                style: TextStyle(
                    color: Color(0xff14142B), fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }
}
