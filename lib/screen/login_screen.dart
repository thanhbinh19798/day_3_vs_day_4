
import 'package:design_pattern_vs_view_advance/home_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginScreen extends GetWidget<HomeController> {
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();

  final HomeController controllerHome = Get.put(HomeController());

  LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      Scaffold(
        appBar: AppBar(
          title: const Text("Login page"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextField(
                controller: email,
                onChanged: (value) {
                  controllerHome.validateUsername(value);
                },
                decoration: InputDecoration(
                  hintText: 'Email | Phone Number',
                  hintStyle: const TextStyle(color: Colors.grey),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Colors.black),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: Colors.blue)),
                  isDense: true,
                  contentPadding: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                ),
                cursorColor: Colors.black,
                style: const TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextField(
                controller: pass,
                obscureText: true,
                onChanged: (value) {
                  controllerHome.validatePassword(value);
                },
                decoration: InputDecoration(
                  hintText: 'Password',
                  hintStyle: const TextStyle(color: Colors.grey),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Colors.black),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: Colors.blue)),
                  isDense: true,
                  // Added this
                  contentPadding: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                ),
                cursorColor: Colors.black,
                style: const TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                controllerHome.navigateHomeScreen(context);
              },
              child: const Text("LOGIN"),
            ),
          ],
        ),
      );

  showSnackBar(BuildContext context) {
    final snackBar = SnackBar(
      content: const Text('Must fill Username, password!'),
      action: SnackBarAction(
        label: 'Undo',
        onPressed: () {

        },
      ),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

}