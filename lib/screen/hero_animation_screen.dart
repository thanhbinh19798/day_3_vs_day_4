
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../model/category_model.dart';

class HeroExamplePage extends StatelessWidget {
  final int index;

  const HeroExamplePage({super.key, required this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Selected image '),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Hero(
                tag: 'flutterLogo $index',
                child: SvgPicture.asset(
                  category[index].image,
                  width: 200,
                  height: 200,
                )),
            const SizedBox(
              height: 20,
            ),
            Text(
              category[index].title,
              style: const TextStyle(fontSize: 30),
            )
          ],
        ),
      ),
    );
  }
}