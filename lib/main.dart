import 'package:design_pattern_vs_view_advance/home_controller.dart';
import 'package:design_pattern_vs_view_advance/screen/list_page_note.dart';
import 'package:design_pattern_vs_view_advance/screen/login_screen.dart';
import 'package:design_pattern_vs_view_advance/screen/picker_file_screen.dart';
import 'package:design_pattern_vs_view_advance/view/detail_action.dart';
import 'package:design_pattern_vs_view_advance/view/detail_highlight_information.dart';
import 'package:design_pattern_vs_view_advance/view/detailbase_information.dart';
import 'package:design_pattern_vs_view_advance/view/listview_detail.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'view/category.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final HomeController homeController = Get.put(HomeController());

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      ),
      home: Obx(() =>
          homeController.isLogin.value == true ? MyHomePage() : LoginScreen()),

      // MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);

  final HomeController homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Demo Home Page'),
        actions: [
          IconButton(
              icon: SvgPicture.asset("assets/menu_option.svg"),
              onPressed: () {
                homeController.setVisible();
              })
        ],
      ),
      body: SafeArea(
          child: Scaffold(
        body: Stack(
          children: [
            PageView(
              controller: homeController.pageViewController,
              onPageChanged: homeController.selectedIndex,
              children: [
                ExtendedNestedScrollView(
                  controller: homeController.scrollController,
                  physics: const ClampingScrollPhysics(),
                  headerSliverBuilder:
                      (BuildContext context, bool innerBoxIsScrolled) {
                    return [
                      SliverToBoxAdapter(
                        key: homeController.centerKey,
                        child: Column(
                          children: const [
                            Category(),
                            SizedBox(
                              height: 30,
                            ),
                            ListViewDetail(),
                          ],
                        ),
                      ),
                      const SliverToBoxAdapter(
                        child: SizedBox(
                          height: 15,
                        ),
                      ),
                      SliverStickyHeader(
                        header: Container(
                          height: 30,
                          margin: const EdgeInsets.only(left: 20, right: 20),
                          child: TabBar(
                            onTap: (value) {
                              homeController.position.value = value;
                            },
                            controller: homeController.tabController,
                            labelColor: Colors.black,
                            unselectedLabelColor: Colors.grey,
                            indicatorColor: Colors.transparent,
                            indicator: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.circular(20)),
                            tabs: const <Widget>[
                              Tab(
                                  child: Text('Cơ bản',
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400,
                                      ))),
                              Tab(
                                  child: Text('Đặc trưng',
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400,
                                      ))),
                              Tab(
                                  child: Text('Tương tác',
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400,
                                      ))),
                            ],
                          ),
                        ),
                      ),
                    ];
                  },
                  pinnedHeaderSliverHeightBuilder: () {
                    return 45;
                  },
                  body: TabBarView(
                      controller: homeController.tabController,
                      children: const <Widget>[
                        CustomScrollView(
                          slivers: [
                            SliverToBoxAdapter(
                              child: DetailBaseInformation(),
                            )
                          ],
                        ),
                        CustomScrollView(
                          slivers: [
                            SliverToBoxAdapter(
                                child: DetailHighLightInformation())
                          ],
                        ),
                        CustomScrollView(
                          slivers: [
                            SliverToBoxAdapter(
                              child: DetailAction(),
                            )
                          ],
                        ),
                      ]),
                ),
                const ListPageNote(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    PickerFileScreen()
                  ],
                )
              ],
            ),
            Obx(() => AnimatedContainer(
                  curve: Curves.ease,
                  height:
                      homeController.isVisible.value == true ? Get.height : 0,
                  duration: const Duration(milliseconds: 700),
                  child: Stack(
                    children: [
                      Positioned(
                          top: 0,
                          left: 0,
                          bottom: 0,
                          right: 0,
                          child: GestureDetector(
                              onTap: () {
                                homeController.setVisible();
                              },
                              child: Container(
                                  color: const Color(0xFF000000)
                                      .withOpacity(0.7)))),
                      Positioned(
                          child: Container(
                        height: Get.height / 3,
                        width: double.infinity,
                        color: Colors.blue,
                        child: SingleChildScrollView(
                            child: Container(
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(top: 20),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                            ),
                            onPressed: () {
                              homeController.initLogout();
                            },
                            child: const Text('Logout',
                                style: TextStyle(color: Colors.black)),
                          ),
                        )),
                      ))
                    ],
                  ),
                ))
          ],
        ),
        bottomNavigationBar: Obx(
          () => BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.business),
                label: 'Business',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.school),
                label: 'School',
              ),
            ],
            onTap: homeController.onItemTapped,
            currentIndex: homeController.selectedIndex.value,
            selectedItemColor: Colors.amber[800],
          ),
        ),
      )),
    );
  }
}
