import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../screen/hero_animation_screen.dart';
import '../model/category_model.dart';
import '../home_controller.dart';

class Category extends GetWidget<HomeController> {
  const Category({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      margin: const EdgeInsets.only(left: 16, right: 16),
      child: Card(
        color: const Color(0xFFFFFFFF),
        margin: const EdgeInsets.fromLTRB(15, 15, 15, 0),
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ...List.generate(
                  5,
                  (index) => GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HeroExamplePage(
                                        index: index,
                                      )));
                        },
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Hero(
                                  tag: 'flutterLogo $index',
                                  child: SvgPicture.asset(
                                    category[index].image,
                                  )),
                              const SizedBox(height: 14),
                              Text(
                                category[index].title,
                                style: const TextStyle(
                                  fontSize: 14,
                                  fontFamily: "Rotobo",
                                  fontWeight: FontWeight.w400,
                                ),
                              )
                            ]),
                      ))
            ],
          ),
        ),
      ),
    );
  }
}
