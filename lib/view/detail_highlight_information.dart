import 'package:design_pattern_vs_view_advance/home_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'common_messenger.dart';

class DetailHighLightInformation extends GetWidget<HomeController> {
  const DetailHighLightInformation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 15, right: 15, top: 12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: const Color(0xFFFFFFFF)),
      child: detailBaseView(),
    );
  }

  Widget detailBaseView() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          margin: const EdgeInsets.only(right: 10),
          child: Column(
            children: [
              baseRowGroupText("Sản phẩm"),
              const SizedBox(
                height: 21,
              ),
              baseRowGroupText("Cơ hội hợp tác"),
              const SizedBox(
                height: 21,
              ),
              Container(
                  height: 1,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      color: Color(0xFFD9D9D9))),
              const SizedBox(
                height: 21,
              ),
              baseRowGroupText("Loại hình dự án"),
              const SizedBox(
                height: 21,
              ),
              baseRowGroupText("Dự án đang bán"),
              const SizedBox(
                height: 21,
              ),
              baseRowGroupText("Doanh thu/tháng"),
              const SizedBox(
                height: 21,
              ),
              baseRowGroupText("Số lượng thành viên"),
              const SizedBox(
                height: 21,
              ),
              Container(
                  height: 1,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      color: Color(0xFFD9D9D9))),
              const SizedBox(
                height: 21,
              ),
              baseRowGroupText("Vấn đề gặp phải"),
              const SizedBox(
                height: 21,
              ),
              baseRowGroupText("Mong muốn khách hàng"),
              const SizedBox(
                height: 21,
              ),
              Container(
                  height: 1,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      color: Color(0xFFD9D9D9))),
            ],
          ),
        ),

        Container(
            margin: const EdgeInsets.only(left: 15),
            child: const CommonMessenger()
        ),
        const SizedBox(
          height: 15,
        ),
      ],
    );
  }

  Widget defaultTextGray(String text) {
    return Text(text,
        style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Color(0xFF999999)));
  }

  Widget textDefineBase(String text, {bool isBlue = false}) {
    return Text(text,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w400,
          color: isBlue == true
              ? const Color(0xFF007AFF)
              : const Color(0xFF14142B),
        ));
  }

  Widget textBase(String text) {
    return Text(
      text.toUpperCase(),
      style: const TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: Color(0xFF6E7191),
      ),
    );
  }

  Widget baseRowGroupText(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        textBase(text),
        Row(
          children: [
            defaultTextGray("--"),
            const SizedBox(
              width: 13,
            ),
            SvgPicture.asset("assets/drop_down_gray.svg")
          ],
        )
      ],
    );
  }
}


