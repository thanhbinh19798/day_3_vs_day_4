import 'package:design_pattern_vs_view_advance/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../model/live_note_model.dart';
import 'detail_voice.dart';


class DetailAction extends GetWidget<HomeController> {
  const DetailAction({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Container(
          alignment: Alignment.topLeft,
          margin: const EdgeInsets.only(left: 15, right: 15, top: 12),
          padding: const EdgeInsets.only(left: 15, top: 15),
          width: double.infinity,
          height: 247,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: const Color(0xFFFFFFFF)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Lịch sử bản ghi",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Color(0xFF14142B)),
              ),
              const SizedBox(
                height: 5,
              ),
              Container(
                width: 150,
                height: 26,
                decoration: BoxDecoration(
                  color: const Color(0xFFEDEDED),
                  borderRadius: BorderRadius.circular(36),
                ),
                child: const Center(child: Text("2022 -06 -26")),
              ),
              const SizedBox(
                height: 15,
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(bottom: 20, right: 10),
                  child: RawScrollbar(
                    radius: const Radius.circular(10),
                    thickness: 10,
                    thumbColor: Colors.black12,
                    thumbVisibility: true,
                    controller: controller.scrollControllerHistoryList,
                    child: ListView.builder(
                        controller: controller.scrollControllerHistoryList,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: live.length,
                        itemBuilder: (context, index) =>
                            itemBuilder(live[index], index)),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          height: 70,
          margin: const EdgeInsets.only(left: 15, right: 15, top: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: const Color(0xFFFFFFFF)),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                child: Row(
                  children: [
                    Image.asset('assets/ring/Ring.png', width: 40,
                      height: 40,
                      fit: BoxFit.contain,),
                    const SizedBox(width: 10,),
                    const Text(
                      "Lịch sử cuộc gọi",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF14142B)),
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Container(
                    width: 28,
                    height: 28,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(29),
                        color: const Color(0xFFCCE4FF)
                    ),
                    child: const Center(
                      child: Text("3", style: TextStyle(
                          fontSize: 14,
                          color: Color(0xFF007AFF),
                          fontWeight: FontWeight.w500
                      ),),
                    ),
                  ),
                  AnimatedBuilder(
                    animation: controller.animation,
                    child: GestureDetector(
                        onTap: () {
                          controller.checkRolation();
                          controller.getVisible();
                        },
                        child: SvgPicture.asset("assets/ArrowRight.svg")),
                    builder: (BuildContext context, Widget? child) {
                      return Transform.rotate(
                        angle: controller.animation.value,
                        child: child,
                      );
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
        const DetailVoice(),
      ],
    );
  }

  Widget itemBuilder(LiveNoteModel liveNoteModel, index) {

    return Container(
      width: double.infinity,
      height: 40,
      margin: index == 0 ? const EdgeInsets.only(top: 10) : null,
      child: Row(
        children: [
          Column(
            children: [
              index == 0
                  ? Image.asset("assets/start_image/history.png",width: 32, height: 32,fit: BoxFit.contain,)
                  : Image.asset("assets/end_image/image_end.png",width: 32, height: 32,fit: BoxFit.contain,),
              index < live.length - 1
                  ? Expanded(
                child: Container(
                  height: 18,
                  width: 1,
                  decoration: const BoxDecoration(color: Colors.grey),
                ),
              )
                  : const SizedBox()
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Expanded(child: Text(liveNoteModel.title, maxLines: 1,)), Expanded(child: Text(liveNoteModel.detail))],
            ),
          )
        ],
      ),
    );
  }

}
