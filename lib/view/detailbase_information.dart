import 'package:flutter/cupertino.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../home_controller.dart';
import 'common_messenger.dart';

class DetailBaseInformation extends GetWidget<HomeController> {
  const DetailBaseInformation({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          margin: const EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textBase(""),
                  RatingBar.builder(
                    itemSize: 25,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) =>
                        SvgPicture.asset("assets/star.svg"),
                    onRatingUpdate: (rating) {},
                  )
                ],
              ),
              const SizedBox(
                height: 21,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textBase("từ kênh"),
                  textDefineBase('Fanpage Facebook')
                ],
              ),
              const SizedBox(
                height: 21,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textBase("Lịch hẹn"),
                  textDefineBase("14:50, 27/09/2022", isBlue: true)
                ],
              ),
              const SizedBox(
                height: 21,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                textBase('Ghi chú'),
                textDefineBase('Khách khó tính')
              ]),
              const SizedBox(
                height: 21,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textBase('Tình trạng'),
                  Container(
                    padding: const EdgeInsets.only(
                        top: 8.5, bottom: 8.5, left: 10, right: 10),
                    height: 32,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Color(0xFFFFEF98)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Đang chăm sóc",
                          style: TextStyle(
                              color: Color(0xFFE98C02),
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                        const SizedBox(width: 6.67),
                        SvgPicture.asset("assets/drop_down.svg"),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        Container(
            height: 1,
            margin: const EdgeInsets.only(left: 15, right: 15),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                color: Color(0xFFD9D9D9))),
        const SizedBox(
          height: 15,
        ),
        Container(
          margin: const EdgeInsets.only(left: 15, right: 14),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textBase("Giới tính"),
                  Row(
                    children: [
                      textDefineBase("Nam"),
                      const SizedBox(
                        width: 13,
                      ),
                      SvgPicture.asset("assets/drop_down_gray.svg")
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textBase("Điện thoại chính"),
                  Row(
                    children: [
                      textDefineBase("0984512323"),
                      const SizedBox(
                        width: 13,
                      ),
                      SvgPicture.asset("assets/drop_down_gray.svg")
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              baseRowGroupText('Điện thoại khác'),
              const SizedBox(
                height: 15,
              ),
              baseRowGroupText('email'),
              const SizedBox(
                height: 15,
              ),
              baseRowGroupText('địa chỉ'),
              const SizedBox(
                height: 15,
              ),
              baseRowGroupText('ngành nghề'),
              const SizedBox(
                height: 15,
              ),
              const SizedBox(
                height: 15,
              ),
              const SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
        Container(
            height: 1,
            margin: const EdgeInsets.only(left: 15, right: 15),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                color: Color(0xFFD9D9D9))),
        const SizedBox(
          height: 15,
        ),
        Container(
          margin: const EdgeInsets.only(right: 35, left: 15),
          child: Column(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                textBase('Người tạo'),
                textDefineBase('Administrator')
              ]),
              const SizedBox(
                height: 15,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Expanded(child: textBase('Người người phụ trách')),
                textDefineBase('Administrator')
              ]),
              const SizedBox(
                height: 15,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: [
                textBase('Thời gian tạo'),
                textDefineBase('17:00 29/06/2022')
              ]),
              const SizedBox(
                height: 15,
              ),
              Row(
                children: [
                  SvgPicture.asset("assets/time.svg"),
                  const SizedBox(width: 6,),
                  const Expanded(
                    child: Text('Cập nhập lần cuối lúc: 23/05/2022',
                      style: TextStyle(fontSize: 14,
                          color: Color(0xFF999999)),),
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
        Container(
            height: 1,
            margin: const EdgeInsets.only(left: 15, right: 15),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                color: Color(0xFFD9D9D9))),
        const SizedBox(
          height: 15,
        ),
        Container(margin: const EdgeInsets.only(left: 15), child: const CommonMessenger()),
        const SizedBox(
          height: 15,
        ),
      ],
    );
  }

  Widget textBase(String text) {
    return Text(
      text.toUpperCase(),
      maxLines: 1,
      style: const TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: Color(0xFF6E7191),
      ),
    );
  }

  Widget textDefineBase(String text, {bool isBlue = false}) {
    return Text(text,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w400,
          color: isBlue == true
              ? const Color(0xFF007AFF)
              : const Color(0xFF14142B),
        ));
  }

  Widget baseRowGroupText(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        textBase(text),
        Row(
          children: [
            defaultTextGray("--"),
            const SizedBox(
              width: 13,
            ),
          ],
        )
      ],
    );
  }

  Widget defaultTextGray(String text) {
    return Text(text,
        style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Color(0xFF999999)));
  }
}