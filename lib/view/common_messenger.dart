import 'package:design_pattern_vs_view_advance/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class CommonMessenger extends GetView<HomeController>{
  const CommonMessenger({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            SvgPicture.asset("assets/heart.svg"),
            const SizedBox(width: 7,),
            const Text("1",style: TextStyle(
                fontSize: 12,
                color: Color(0xFF536471)
            ),),
            const SizedBox(width: 14,),
            SvgPicture.asset("assets/messenger_group.svg"),
            const SizedBox(width: 7,),
            const Text('4')
          ],
        ),
        const SizedBox(height: 25,),
        Row(
          children: [
            SvgPicture.asset("assets/cartoon_girl.svg"),
            const SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text('APM Ngọc Anh', style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Color(0xFF0F1419),
                ),),
                Text('5 phút trước', style: TextStyle(
                    fontSize: 10,
                    color: Color(0xFF999999),
                    fontWeight: FontWeight.w400
                ),),
              ],
            )
          ],
        ),
        const SizedBox(height: 5),
        const Text("Ổn đấy em, nhiệt tình lên nhé !", style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            color: Color(0xFF0F1419)
        ),),
        const SizedBox(height: 15,),
        Row(
          children: [
            SvgPicture.asset("assets/cartoon_boy.svg"),
            const SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text('Vũ Hoàng Dũng', style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Color(0xFF0F1419),
                ),),
                Text('5 phút trước', style: TextStyle(
                    fontSize: 10,
                    color: Color(0xFF999999),
                    fontWeight: FontWeight.w400
                ),),
              ],
            )
          ],
        ),
        const SizedBox(height: 5),
        const Text("Dạ vâng ạ", style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            color: Color(0xFF0F1419)
        ),),
        const SizedBox(height: 15),
        Container(
          height: 36,
          decoration: BoxDecoration(
            color: const Color(0xFFEDEDED),
            borderRadius:  BorderRadius.circular(16),
          ),
          child: const TextField(
            decoration: InputDecoration(
              hintText: 'Viết bình luận tại đây',
              hintStyle: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Color(0xFF999999)
              ),
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              isDense: true,
              // Added this
              contentPadding: EdgeInsets.fromLTRB(15, 8, 15, 8),
            ),
          ),
        )
      ],
    );
  }
}