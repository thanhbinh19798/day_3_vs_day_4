
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../home_controller.dart';

class ListViewDetail extends GetWidget<HomeController> {
  const ListViewDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 117,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 3,
          itemBuilder: (context, index) => Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            margin: const EdgeInsets.only(left: 15, top: 1, bottom: 1),
            child: SizedBox(
              width: 325,
              height: 115,
              child: Stack(
                alignment: Alignment.topRight,
                children: [
                  Container(
                    height: 26,
                    width: 104,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25),
                            topRight: Radius.circular(10)),
                        color: Color(0xFFFF3B30)),
                    child: const Text(
                      'Trung bình',
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFFFFFFFF)),
                    ),
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 5, left: 15),
                            height: 24,
                            width: 24,
                            // child: SvgPicture.asset("assets/image_person.svg"),
                          ),
                          Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(top: 5, left: 7.5),
                            height: 26,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color: const Color.fromRGBO(255, 191, 0, 0.2)),
                            child: const Padding(
                              padding: EdgeInsets.only(
                                  left: 5, right: 5, top: 0, bottom: 0),
                              child: Text(
                                'Khách siêu nét',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Rotobo',
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFFF6891F)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            Container(
                              alignment: Alignment.topCenter,
                              margin: const EdgeInsets.only(top: 11, left: 15),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: const Color(0xFF5392FF)),
                                  color: const Color(0xFFE7F2FF)),
                              child: Column(
                                children: [
                                  Container(
                                      alignment: Alignment.center,
                                      height: 16,
                                      width: 55,
                                      color: const Color(0xFF5392FF),
                                      child: const Text(
                                        "Thứ 2",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 12,
                                            color: Color(0xFFFFFFFF)),
                                      )),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  const Text(
                                    "26/9",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20,
                                        color: Color(0xFF007AFF)),
                                  ),
                                  const Text("09:00",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 10,
                                          color: Color(0xFF6E7191))),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        right: 20, bottom: 10),
                                    child: const Text(
                                      'Dẫn anh Cường đến Shophouse Vườn vua Phú Thọ Dẫn anh Cường đến Shophouse Vườn vua Phú Thọ',
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: const [
                                      Text(
                                        "12 comments",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFFA0A3BD)),
                                      ),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        "0 file",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFFA0A3BD)),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
