import 'package:design_pattern_vs_view_advance/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:get/get.dart';

class DetailVoice extends GetWidget<HomeController> {
  const DetailVoice({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => AnimatedContainer(
          height: controller.isCheckVisible.value == true ? 200 : 0,
          curve: Curves.ease,
          color: Color(0xFFEDEDED),
          duration: const Duration(milliseconds: 500),
          child: Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 15, bottom: 15),
            child: Swiper(
              itemCount: 3,
              viewportFraction: 0.85,
              scale: 0.95,
              itemBuilder: (context, index) => SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                  child: Column(
                    children: [
                      Padding(
                        padding:
                        const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Text(
                                  "Phạm Thu Hương - A23",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                ),
                                Text("02:30",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black))
                              ],
                            ),
                            Obx(
                                  () => Slider(
                                  value: controller.sliderValue.value,
                                  min: 0,
                                  max: controller.sliderValueMax.value,
                                  onChanged: (double value) {
                                    controller.sliderValue.value = value;
                                  },
                                  divisions: 150,
                                  label: '${controller.sliderValue.value}'),
                            ),
                            Obx(() => Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    '${controller.sliderValue.value}',
                                    style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF999999)),
                                  ),
                                  Text(
                                      '${controller.sliderValueMax.value - controller.sliderValue.value}',
                                      style: const TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF999999)))
                                ])),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset("assets/pre_button.svg"),
                                const SizedBox(width: 30,),
                                SvgPicture.asset("assets/pause_button.svg"),
                                const SizedBox(width: 30,),
                                SvgPicture.asset("assets/next_button.svg")
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 1,
                        decoration: const BoxDecoration(
                            color: Color(0xFFD9D9D9)
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 15, right: 15, top: 5,bottom: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const [
                            Text("Gọi bởi Vũ Hoàng Dũng", style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Color(0xFF999999)
                            ),),
                            Text("2 giờ trước",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFF999999)))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
