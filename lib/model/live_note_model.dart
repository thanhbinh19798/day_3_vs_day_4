
class LiveNoteModel{
  final int id;
  final String title, detail;

  LiveNoteModel(this.id, this.title, this.detail);

}

List<LiveNoteModel> live = [
  LiveNoteModel(1, "09:50:30 Bạn đã tạo Công việc:", "Chào mời khách"),
  LiveNoteModel(2, "09:47:15 Administrator đã tạo Khách hàng:", "Phạm Thu Hương"),
  LiveNoteModel(3, "09:47:15 Administrator đã tạo Khách hàng:", "Phạm Thu Hương"),
  LiveNoteModel(4, "09:47:15 Administrator đã tạo Khách hàng:", "Phạm Thu Hương"),
  LiveNoteModel(5, "09:47:15 Administrator đã tạo Khách hàng:", "Phạm Thu Hương"),
];