
class CategoryModel{
  final int id;
  final String title, image;

  CategoryModel(this.id, this.title, this.image);

}

List<CategoryModel> category = [
  CategoryModel(1, "SMS", "assets/sms.svg"),
  CategoryModel(2, "Lịch hẹn", "assets/schedule.svg"),
  CategoryModel(3, "Công việc", "assets/job.svg"),
  CategoryModel(4, "Ghim", "assets/pin.svg"), 
  CategoryModel(5, "Share", "assets/share.svg"),
];