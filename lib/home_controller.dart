
import 'dart:math';
import 'package:design_pattern_vs_view_advance/main.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:open_file/open_file.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';


class HomeController extends GetxController with GetTickerProviderStateMixin {

  var position = 0.obs;
  var selectedIndex = 0.obs;
  late TabController tabController;
  late ScrollController scrollController;
  late PageController pageViewController;
  var sliderValue = 0.0.obs;
  var sliderValueMax = 150.0.obs;
  var isVisible = false.obs;
  var isCheckVisible = false.obs;
  late Animation<double> animation;
  var isCheck = true.obs;
  var degrees = 0.0.obs;
  var degreesOff = 0.0.obs;
  late AnimationController animationController;
  late ScrollController scrollControllerHistoryList;


  Key centerKey = const ValueKey<String>('bottom-sliver-list');


  @override
  void onInit() {
    super.onInit();
    loadStateLogin();
    tabController = TabController(length: 3, vsync: this);
    scrollController = ScrollController();
    pageViewController = PageController();
    scrollControllerHistoryList = ScrollController();
    animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 500));
    animation = Tween<double>(begin: 0, end: degrees.value).animate(animationController);
    tabController.addListener(() {
      position.value = tabController.index;
    });

  }
  void setVisible() {
    isVisible.value = ! isVisible.value;
  }

  void checkRolation() {
    if (isCheck.value) {
      setRotation(180);
      animationController.forward(from: 0);
    } else {
      setRotationOff(180);
      animationController.forward(from: 0);
    }
    isCheck.value = !isCheck.value;
  }

  void getVisible() {
    isCheckVisible.value = !isCheckVisible.value;
  }

  void setRotation(double degreesShape) {
    degrees.value = degreesShape * pi / 180;
    animation = Tween<double>(begin: 0, end: degrees.value).animate(animationController);
  }
  void setRotationOff(double degreesShape) {
    degreesOff.value = degreesShape * pi / 180;
    animation = Tween<double>(begin: degreesOff.value, end: 0).animate(animationController);
  }

  void onItemTapped(int index) {
    selectedIndex.value = index;
    pageViewController.jumpToPage(index);
  }

  void permissionHelper(BuildContext context) async {
    PermissionStatus storageStatus = await Permission.storage.request();
    if(storageStatus == PermissionStatus.granted) {
      _openFilePicker(context);
    }
    if(storageStatus == PermissionStatus.denied) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Permission not granted")));
    }

    if(storageStatus == PermissionStatus.permanentlyDenied) {
      openAppSettings();
    }
  }

  Future<void> _openFilePicker(BuildContext context) async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.any,
      );
      if (result != null && result.files.single.path != null) {
        final resultFirst = result.files.first;
        OpenFile.open(resultFirst.path);

      }
    } on PlatformException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Unsupported operation + $e")));
    } catch (ex) {
      print(ex);
    }
  }

  var isLogin = false.obs;
  var isValidateUser = false.obs;
  var isValidatePW = false.obs;

  void loadStateLogin() async {
    final stateLogin = await SharedPreferences.getInstance();
    isLogin.value = stateLogin.getBool('isLogin') ?? false;
  }

  void initLogin() async {
    final stateLogin = await SharedPreferences.getInstance();
    isLogin.value = true;
    stateLogin.setBool('isLogin', isLogin.value);
  }

  void initLogout() async {
    final stateLogin = await SharedPreferences.getInstance();
    isLogin.value = false;
    stateLogin.setBool('isLogin', isLogin.value);
  }

  void validateUsername(String username) {
    if(username.isNotEmpty && username.contains("@")) {
      isValidateUser.value = true;
    } else {
      isValidateUser.value = false;
    }
  }

  void validatePassword(String password) {
    if(password.length >= 6) {
      isValidatePW.value = true;
    } else {
      isValidatePW.value = false;
    }
  }

  void navigateHomeScreen(BuildContext context) {
    if(isValidateUser.value == true && isValidatePW.value == true) {
      initLogin();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Must fill Username contain "@", password has length higher 5'),));
    }
  }

  double _animatedHeight = 200.0;

  double setHeight() {
    return _animatedHeight!=0.0?_animatedHeight=0.0:_animatedHeight=200.0;
  }
}